//Get Posts
const getData = () => {
	fetch("https://jsonplaceholder.typicode.com/users")
		.then((response)=>{
		return response.json();
	}).then((data)=>{
		let posts = "";
		data.forEach((post)=>{
			posts += `
				<div class="col-4 border border-secondary">
					<img class="img-fluid" width="350px" src="https://dynaimage.cdn.cnn.com/cnn/c_fill,g_auto,w_1200,h_675,ar_16:9/https%3A%2F%2Fcdn.cnn.com%2Fcnnnext%2Fdam%2Fassets%2F210929133837-squid-game-netflix-03.jpeg"/>
					<h2>Name: ${post.name}</h2>
					<p>Email: ${post.email}</p>
					<p>Company: ${post.company.name}</p>
					<p>Phone: ${post.phone}</p>
				</div>`
		})
		document.querySelector("#output").innerHTML = posts;
	})
	.catch((err)=>{
		console.log("rejected");
	})
}

//Add Post
const addPost = (e) =>{
	e.preventDefault(); //stops submit from doing its default function - submitting the form somewhere
	const name = document.querySelector("#name").value;
	const username = document.querySelector("#username").value;
	const email = document.querySelector("#email").value;
	const street = document.querySelector("#street").value;
	const suite = document.querySelector("#suite").value;
	const city = document.querySelector("#city").value;
	const zipcode = document.querySelector("#zipcode").value;
	const geoLat = document.querySelector("#geoLat").value;
	const geoLong = document.querySelector("#geoLong").value;
	const phone = document.querySelector("#phone").value;
	const website = document.querySelector("#website").value;
	const compName = document.querySelector("#compName").value;
	const compPhrase = document.querySelector("#compPhrase").value;
	const compBS = document.querySelector("#compBS").value;

	fetch("https://jsonplaceholder.typicode.com/users",{
		method: "POST",
		headers: {
			"Accept": "application/json, text/plain",
			"Content-type": "application/json"
		},
		body:JSON.stringify({
			name: name,
			username: username,
			email: email,
			address: {
				street: street,
				suite: suite,
				city: city,
				zipcode: zipcode,
				geo: {
					lat: geoLat,
					lng: geoLong
				}
			},
			phone: phone,
			website: website,
			company: {
				name: compName,
				catchPhrase: compPhrase,
				bs: compBS
			}
		})
	})
	.then(response=>response.json())
	.then(data=>console.log(data))
}


const btn1 = document.querySelector("#btn1");
btn1.addEventListener("click",getData);

const addForm = document.querySelector("#addPost");
addForm.addEventListener("submit",addPost);